<?

/**
 *
 *      ЗАДАНИЕ
 *  Разработать приложение, которое на основной странице выводит список новостей, хранящихся в массиве $articles.
 *
 *  Страница должна содержать:
 *      - Заголовок приложения;
 *      - Дата последней публикации материала;
 *      - Мета-информацию (title, description, keywords и др.).
 *
 *  Карточка новости должна содержать следующие элементы:
 *      - Заголовок новости;
 *      - Анонс новости (макс. 250 символов, обрезать до окончания последнего слова. Анонс должен оканчиваться многоточием);
 *      - Дата новости в формате 01.01.2001 (Дата доступна по ключу "time" в формате timestamp);
 *      - Ссылка на подробную страницу новости. На данный момент оставить нефункциональной.
 *
 *  Во время разработки использовать систему контроля версий GIT, как минимум 3 коммита
 *  Выбор фреймворков и библиотек остается за разработчиком.
 *  Верстка должна соответствовать современным семантическим требованиям, остальное на усмотрение разработчика.
 *  
 *  Плючом будет комментирование кода.
 * 
 */

$articles = array(
    array(
        "id" => 1,
        "time" => time() - pow(1, 5) * 1000,
        "title" => "Title 01",
        "text" => "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sed placeat similique eaque incidunt quia, odio debitis quae. Id, tenetur voluptas omnis architecto pariatur esse, repudiandae illum voluptates repellendus commodi culpa!
        Aperiam perferendis nesciunt nemo consectetur sit laboriosam quasi hic possimus nam praesentium corrupti commodi inventore ut harum, at fugit mollitia sequi. Fugiat alias odit id distinctio odio sapiente quidem non.
        Totam repudiandae ut omnis corporis eum eos, officiis accusamus cupiditate! Asperiores, aut est minus quaerat at maxime labore atque ex sapiente vel ipsum molestias enim quo laudantium iusto amet doloribus!
        Distinctio beatae dolorum doloribus dolores voluptatum commodi quibusdam eum, provident maiores recusandae velit quisquam nobis esse? Ea ratione corporis natus accusamus perferendis, voluptate provident repudiandae pariatur repellat deleniti, libero veritatis?
        Quod neque eum, vero sapiente dignissimos dolores enim quos praesentium repellendus natus odit porro voluptate inventore officia odio asperiores, magni mollitia harum accusantium a doloremque cumque, assumenda possimus? Ullam, sint."
    ),
    array(
        "id" => 2,
        "time" => time() - pow(2, 5) * 1000,
        "title" => "Title 02",
        "text" => "Cumque magni et eum aliquid. Reiciendis amet id veritatis adipisci totam? Et dolorem, omnis natus ut dicta necessitatibus recusandae cum dignissimos voluptates, vero officiis illum! Facilis magnam ducimus sapiente pariatur!
        Officia, distinctio iusto. Dolor tempore obcaecati quisquam, vero, quidem culpa ipsum ratione accusamus perspiciatis nemo repellat doloremque! Excepturi cum qui necessitatibus, et corporis atque tempore maxime, distinctio temporibus voluptatum est.
        Labore, dolores illum recusandae quis veniam debitis in pariatur obcaecati, sunt, magnam ab aliquam libero ipsam delectus fugiat cupiditate reiciendis nulla temporibus. Doloremque animi iure accusantium consequuntur reprehenderit consectetur neque.
        Eligendi quibusdam veniam, magni libero inventore quos eveniet sunt in aliquid, nisi ratione delectus voluptates quae. Facere doloremque mollitia amet iste quasi voluptas error repellendus vitae unde a, rerum blanditiis?
        Aut delectus quis repellat harum, sed deserunt cumque labore architecto vel sit est neque dicta similique. Ut aperiam, voluptatibus facilis pariatur accusamus quidem doloribus illum sint laboriosam nulla neque ipsa!
        Sunt voluptatem culpa, dolore eligendi mollitia enim magnam vero! Asperiores expedita dolore modi quo amet. Porro molestias ea mollitia fuga pariatur molestiae cum, quaerat minima eligendi doloribus qui! Sint, sapiente!
        Aliquid reiciendis ut earum accusamus dolores maiores, hic error fugit eveniet autem, dicta recusandae fuga voluptate assumenda iusto optio quas mollitia totam est. Nihil nulla dolore ipsam ducimus illo libero?
        Dolore culpa accusamus autem fuga omnis, non at impedit cumque, illo nisi numquam reprehenderit consectetur similique sed amet. Quas sint quos debitis culpa dolor excepturi fuga eum quam eius earum."
    ),
    array(
        "id" => 3,
        "time" => time() - pow(3, 5) * 1000,
        "title" => "Title 03",
        "text" => "Cumque, molestias ex nesciunt corporis earum voluptas facere amet similique perspiciatis aut recusandae consectetur incidunt quos fuga a quam ipsa sequi quo? Porro corrupti facere cumque sapiente ipsa explicabo modi.
        Ratione sapiente dolore praesentium cumque minus corrupti distinctio ad assumenda nam ab saepe a error corporis repudiandae laudantium, iure impedit optio officia eveniet, inventore dolorem numquam repellendus. Facere, aut blanditiis.
        Cupiditate excepturi, doloribus explicabo suscipit exercitationem labore omnis. Maxime repellendus doloribus dolore dolores quos eum quam error qui explicabo saepe facilis sunt corrupti soluta distinctio, iste odit repellat quibusdam at.
        Optio mollitia, repellendus facere eum debitis adipisci esse quos possimus quia nisi praesentium velit recusandae doloribus pariatur qui quae. Numquam nostrum saepe rem fugit beatae, dicta doloremque unde corrupti aut?
        Voluptates minus, distinctio eum numquam ipsam earum perspiciatis autem fugiat, adipisci placeat impedit itaque quasi dolore nam! Quas assumenda expedita doloribus magni, hic quis dolores voluptate alias dolorum dicta obcaecati!
        Dolorem sint natus aliquam. Eius eligendi eaque voluptatibus officiis labore quam ipsum doloribus rerum. Rem, praesentium saepe aliquam quam ratione labore quaerat consectetur hic voluptatum a ea beatae, facere ullam!
        Voluptas ratione, ipsum ad fuga officia hic aliquam consequuntur similique quasi tempore tenetur qui dolor corporis ut, voluptatibus, minus magnam laborum id voluptate expedita in illo ab? Quisquam, quam? Sapiente?"
    ),
    array(
        "id" => 4,
        "time" => time() - pow(4, 5) * 1000,
        "title" => "Title 04",
        "text" => "Voluptates, consequuntur? Tempora sed veritatis distinctio, aliquam excepturi velit possimus sapiente, alias fugit repellendus blanditiis tempore similique quod unde magnam eveniet magni atque minima ad dolore nihil facilis deserunt odit.
        Quidem inventore dolorum iusto, ad perspiciatis iure assumenda quaerat alias odit soluta sed deserunt. Maiores expedita eius minima minus repellat blanditiis corrupti. Perspiciatis error aliquid laborum eos? Eos, natus perferendis.
        Recusandae omnis quo facere ipsa libero, nulla odit, natus reprehenderit, sit ducimus id. Excepturi fugiat neque nesciunt a impedit deleniti incidunt voluptates maiores! Atque autem quas earum accusamus sequi rerum!
        Fuga aspernatur totam odit architecto dolores veritatis atque sint consequuntur illum, necessitatibus nesciunt modi perspiciatis distinctio quisquam ab soluta, eveniet excepturi molestias nulla. Quam quae exercitationem illo omnis eum deleniti!
        Incidunt maiores necessitatibus alias perspiciatis ratione dignissimos eveniet recusandae consequatur voluptate pariatur sed explicabo iste, consectetur repudiandae culpa, animi sapiente nihil, error dolor? Ea quidem dignissimos, quas commodi vitae sapiente.
        Fugiat corporis eveniet nihil odio quibusdam debitis officiis tempora, accusamus, sit veritatis, cumque quam obcaecati ullam incidunt tempore? Voluptas exercitationem aliquam odit provident libero eum quisquam laboriosam consequatur! Pariatur, eius.
        Consectetur, cupiditate earum. Sit consequuntur nulla odit perspiciatis consequatur voluptatum iste fugiat ad? Odio soluta aut ratione, inventore commodi unde odit ipsa ab, voluptate nemo maxime ipsum necessitatibus veritatis iusto!."
    ),
    array(
        "id" => 5,
        "time" => time() - pow(5, 5) * 1000,
        "title" => "Title 05",
        "text" => "Nostrum cupiditate, reprehenderit, quos eaque temporibus accusamus magnam maiores sed, quia ex nulla quo soluta excepturi vitae quaerat molestiae distinctio eveniet. Architecto ab corporis ipsa odit exercitationem. Voluptas, consequuntur doloremque?
        Autem atque in ab, quasi tempora soluta molestiae, aspernatur magni maxime eveniet molestias rem iure voluptas iusto? Harum dolores nisi at distinctio unde maxime quaerat accusantium minus quod. Explicabo, reprehenderit.
        Distinctio ab eius architecto a cupiditate quibusdam dolor consequuntur aspernatur voluptatibus sapiente repudiandae tempore eaque nisi debitis ut id quasi possimus, in natus, eum totam. Asperiores natus tempora nemo inventore!
        Suscipit, molestias. Voluptatum quod officiis eligendi tempore molestias ratione vitae est placeat amet quisquam. Minima maxime repudiandae recusandae facere ex consequatur voluptatem obcaecati, consequuntur error minus porro, esse maiores laudantium?
        Reprehenderit necessitatibus quos totam quibusdam cumque! Iste necessitatibus vel quos totam cum cumque ad eveniet tenetur officiis odio, consequatur expedita cupiditate, esse ipsum et fugiat dolor nihil excepturi alias dolorem."
    ),
    array(
        "id" => 6,
        "time" => time() - pow(6, 5) * 1000,
        "title" => "Title 06",
        "text" => "Molestiae sint alias, necessitatibus quaerat quod obcaecati officia quam perferendis eaque. Eaque assumenda neque error omnis deserunt dolores, dignissimos quod enim ducimus quisquam quam ipsum doloribus accusantium eum adipisci officia?
        Quos dolor exercitationem odio temporibus earum ipsa modi quasi corporis aliquam rem deleniti quod natus, hic expedita odit officia a eligendi enim inventore optio, beatae, assumenda sit commodi? Velit, provident!
        Ea culpa dolorum hic? Fuga perspiciatis ducimus eaque aspernatur labore necessitatibus atque delectus sed distinctio ad magni modi mollitia omnis voluptatem, minima sequi earum id saepe enim totam odit voluptatibus.
        Cum laborum mollitia, explicabo impedit voluptates eos beatae voluptate. Facere, incidunt possimus similique eum voluptas numquam qui sunt. Similique possimus doloribus repellendus ipsum praesentium numquam mollitia alias, totam neque necessitatibus?
        Dicta inventore dolore, minus dolor, id porro enim a nesciunt et in ipsum facilis qui ea quam necessitatibus iusto pariatur, perspiciatis quia quisquam? Asperiores, commodi voluptatem quia quibusdam atque distinctio.
        Debitis est ex quod similique nisi, ipsam laborum veritatis repellat, sapiente nostrum voluptate suscipit quisquam provident iste inventore fugiat corporis unde dignissimos illo perspiciatis cupiditate recusandae nihil totam. Error, dolor.
        Aperiam et eum facere optio fuga molestias, quasi maxime ipsa, deleniti exercitationem eos voluptatem! Non maxime cupiditate officiis facilis sequi, deserunt vel vitae dolores laboriosam sapiente praesentium, hic asperiores autem?"
    ),
    array(
        "id" => 7,
        "time" => time() - pow(7, 5) * 1000,
        "title" => "Title 07",
        "text" => "Quae voluptatem aut magnam, tenetur dolorem sint repudiandae perspiciatis consequatur? Similique, doloribus blanditiis perspiciatis, tempora incidunt, impedit necessitatibus architecto quo iusto laborum alias vel. Odit tempore dicta repellat neque? Esse?
        Voluptatibus iste reprehenderit voluptates temporibus porro vel provident eum similique alias libero, veniam sed animi doloribus iure adipisci quia commodi amet consectetur? Est voluptatibus ut laborum quam corrupti, velit saepe.
        Veniam ad rerum rem, a iusto at tempore reprehenderit perspiciatis animi incidunt? Amet temporibus eaque molestiae ex, incidunt sint eveniet in, libero ut, nam odio vitae non quo et illo?
        Ipsum placeat illum odit commodi! Maiores aliquid necessitatibus dolores nostrum explicabo similique neque dolore at quidem harum ipsa excepturi laborum sit omnis, minus illo optio dolorum ab sequi, rem temporibus.
        Dolorem, reprehenderit animi quae fuga id quis modi, architecto tempore quisquam, voluptas rerum sunt deleniti veritatis? Temporibus repudiandae nemo perspiciatis sunt ab mollitia pariatur laudantium, omnis atque cumque velit porro.
        Aut deleniti ipsa voluptates assumenda, commodi enim molestiae quod asperiores cumque culpa itaque sint excepturi ullam blanditiis quam error porro totam at necessitatibus soluta. Magni totam architecto sit eveniet commodi.
        Impedit nesciunt vitae officia neque ipsam laborum praesentium nihil omnis iste unde nisi ad, minima odio, ex, corrupti rerum vero. Officia distinctio porro possimus reprehenderit illo dolore necessitatibus debitis alias.
        Commodi deleniti praesentium eveniet dolores reiciendis quos eaque possimus quasi dolore molestiae quisquam asperiores perferendis impedit ab quam enim rem aliquid tempora, labore hic! Veniam nesciunt iste omnis nam iusto.
        Ipsam labore fuga adipisci sapiente maxime ullam harum? Ex esse quos asperiores itaque rerum veniam, fuga porro et placeat, at hic iure id laboriosam quam cupiditate neque sit magnam culpa?"
    ),
    array(
        "id" => 8,
        "time" => time() - pow(8, 5) * 1000,
        "title" => "Title 08",
        "text" => "Incidunt magni voluptatem eaque perspiciatis accusantium assumenda, veniam cum officiis. Quae sequi quod labore vel molestias! Ullam, accusantium aspernatur quia maiores ipsa molestiae autem, distinctio aliquid similique repudiandae, quod atque.
        Molestias ea aperiam, accusantium quidem quam sed nobis earum eligendi delectus natus officiis, ipsam tempora odit eum. Repellat, atque. Nesciunt quos corrupti eius, perspiciatis nulla explicabo ea saepe perferendis odit!
        Laudantium dolorum aspernatur est error quod, non debitis ipsam beatae eaque. Iste provident vero iure labore ea vitae laudantium, similique distinctio ipsam modi fuga nulla doloremque et pariatur cum neque.
        Culpa hic rem ut quam sequi maxime blanditiis quia velit. Quae eius consequatur ut, amet ea dolor delectus! Vitae sequi dolorum id ut ipsam placeat quod eius rerum aut? Voluptas?"
    ),
    array(
        "id" => 9,
        "time" => time() - pow(9, 5) * 1000,
        "title" => "Title 09",
        "text" => "At ad natus voluptatibus, optio animi laborum placeat ex laudantium quibusdam, soluta velit omnis rem fugit ullam magni id qui repudiandae ut neque assumenda culpa eligendi. Neque ipsum obcaecati mollitia?
        Asperiores, nam maxime, omnis maiores ipsam accusantium nemo vero harum doloribus molestiae minus, voluptate quae laudantium sunt totam cum iure ipsum dicta commodi voluptatum corporis! Illum tempora blanditiis expedita iusto.
        Possimus maxime asperiores quam, labore aperiam quos eius. Repellendus eligendi laudantium, officiis tenetur quo aspernatur facilis. Qui doloremque eius, ad blanditiis tempora veniam! A culpa fugiat ab voluptate dolore expedita.
        Quo accusamus consequuntur quam neque unde nam, dolorem incidunt blanditiis aspernatur sunt atque impedit officia ad exercitationem id libero tempora perspiciatis illo eos error repellat? Culpa suscipit aperiam inventore facere!
        Porro enim doloribus nobis odio quod consectetur quibusdam sequi ea magnam accusantium, fugiat optio, distinctio velit quaerat voluptates repellendus possimus soluta perspiciatis, pariatur adipisci! Animi consequatur quidem sapiente facilis sed.
        Doloribus, quos eos. Assumenda ipsam quidem aliquam culpa quas odio dicta quasi maxime molestiae quos fugit illo perferendis excepturi, quibusdam tempora similique. Libero similique alias, exercitationem quis necessitatibus asperiores ipsa!
        Ducimus aliquid eum, repudiandae, aperiam mollitia earum veritatis, officia adipisci delectus cumque labore corrupti porro molestiae sit! Eveniet quaerat molestiae delectus, doloremque quas adipisci quibusdam incidunt, praesentium neque labore porro?
        Consectetur ipsa natus cupiditate maiores atque illo eum. Qui architecto velit blanditiis error commodi praesentium ab necessitatibus quae reprehenderit voluptas vitae, inventore sit eaque perferendis magni consequatur sunt harum unde!
        Reprehenderit quae earum neque vero, autem accusantium quisquam natus qui nisi saepe eos iusto totam consequatur doloremque inventore dicta quod molestias! Dignissimos fugiat, accusantium in voluptates error laborum eius ducimus?"
    ),
    array(
        "id" => 10,
        "time" => time() - pow(10, 5) * 1000,
        "title" => "Title 010",
        "text" => "Illo ut odio, officia nemo quam est. Iste aspernatur, quos architecto, quis ad delectus voluptatibus consequuntur facilis doloribus ipsa dolores quam culpa obcaecati. Error aut iusto dicta asperiores, culpa modi.
        Corporis ullam blanditiis, quasi veniam accusamus perspiciatis omnis facere ab, incidunt ipsum nulla deserunt asperiores error atque ipsa quae aut veritatis cumque id nemo vitae sequi. Neque a excepturi natus.
        Eaque tempora magnam hic quia totam doloribus? Pariatur error labore ratione perspiciatis odio optio, quibusdam ex a, voluptates quis explicabo quo et temporibus, possimus nesciunt est repellat sint sequi autem.
        Cumque dicta necessitatibus, architecto natus pariatur eos, saepe odio ratione sapiente nobis temporibus ea. Corporis placeat tempore sunt architecto, iste nemo, officiis saepe quod sit ut quis veritatis quibusdam omnis!
        Possimus accusamus repellat earum alias sequi enim dignissimos labore facilis culpa! Est reprehenderit, distinctio ipsam veritatis eum ducimus id. Tenetur at, tempore itaque quae quo dolores nihil repudiandae nesciunt quibusdam.
        Et quibusdam quae facere illum dolorem possimus aperiam quidem, sint tempora ut assumenda nesciunt nostrum iste cum rerum consequuntur consectetur quaerat cumque aliquid iure, vel doloribus saepe accusantium! Fugiat, culpa."
    ),
    array(
        "id" => 11,
        "time" => time() - pow(11, 5) * 1000,
        "title" => "Title 011",
        "text" => "Quae quo voluptas optio consectetur corporis? Qui quae dolore tempora sit? Quis quia repudiandae, inventore deserunt fuga sint corporis id, accusamus temporibus aspernatur architecto dolorum dignissimos qui voluptatum quae quidem.
        Dicta earum nisi inventore. Fuga, recusandae. Ipsum, adipisci laboriosam! Eligendi dolores eaque corporis aperiam doloribus commodi eveniet quam perferendis consequatur tenetur, tempora pariatur facilis quo labore reprehenderit officia. Soluta, tempore?
        Distinctio, corrupti tempora libero dolor reprehenderit nihil beatae non minima possimus eius corporis aspernatur pariatur odio veniam, modi velit incidunt. Illum non eaque maxime error in. Dolore repellendus sit assumenda.
        Repellat non id nisi veritatis facilis deserunt quas odit perferendis temporibus, ducimus, dolorem ullam consequuntur quaerat nulla, sunt neque corrupti! Iste quos ut tempore quidem quo asperiores cumque amet modi!
        Blanditiis quisquam odio modi libero quod perspiciatis, iure sunt quam rem ut repellendus unde aperiam sapiente et ullam necessitatibus consequuntur voluptas sit voluptatibus beatae qui natus minima non! Vero, obcaecati.
        Fugiat doloribus maxime quos, sunt obcaecati reiciendis soluta expedita omnis quidem eligendi, culpa, quo cum quod. Inventore omnis laborum soluta, molestias, natus saepe eum porro, aperiam tempora et repellat ipsa.
        Consectetur corrupti consequatur, dicta, ex, temporibus reprehenderit est eaque necessitatibus ab quia aut incidunt natus nobis velit similique in? Pariatur doloremque quibusdam vero suscipit velit accusamus dolorum labore impedit doloribus?
        Nam ut vitae, amet praesentium molestiae illo, harum asperiores pariatur consequuntur unde eum distinctio repellat. Numquam officia sapiente mollitia, earum quam dicta? Quos mollitia, sed natus laborum iure eos placeat!"
    ),
);
?>
